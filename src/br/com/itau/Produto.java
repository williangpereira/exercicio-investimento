package br.com.itau;

import java.util.Scanner;

public class Produto {

    private String nome;
    private int tempoInvestimento;
    private double capitalInvestido;
    private double taxa;
    private double retorno;

    public Produto() {}


    public Produto(String nome, int tempoInvestimento, double capitalInvestido, double taxa ) {

        this.nome = nome;
        this.tempoInvestimento = tempoInvestimento;
        this.capitalInvestido = capitalInvestido;
        this.taxa = 0.0007;

//        System.out.println("nome: " + nome + " Tempo Investimento: " + tempoInvestimento +
//                           " Capital Investido: " + capitalInvestido + " Taxa: " + taxa);
        double retorno = (tempoInvestimento * taxa) * capitalInvestido;

    }


    public String getNome() {
        return nome;
    }

    public double getTempoInvestimento() {
        return tempoInvestimento;
    }

    public double getCapitalInvestido() {
        return capitalInvestido;
    }


    public double getTaxa() {
        return taxa;
    }

    public double getRetorno() {
        return taxa;
    }

}
