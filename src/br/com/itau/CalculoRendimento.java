package br.com.itau;


public class CalculoRendimento {

    private Produto produto;

    public CalculoRendimento(Produto produto){

        String nome = produto.getNome();
        double capital = produto.getCapitalInvestido();
        double taxa = produto.getTaxa();
        double tempoInvestimento = produto.getTempoInvestimento();

        double retornoInvestimento = capital * (tempoInvestimento * taxa);

        System.out.println("Investimento do produto" + nome + " calculado com sucesso!");

    }
}
