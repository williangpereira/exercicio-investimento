package br.com.itau;

public class Impressora {

    Produto produto;

    public Impressora(Produto produto) {

        System.out.println(
                " nome: " + produto.getNome() + '\n' +
                " Tempo Investimento: " + produto.getTempoInvestimento() +'\n' +
                " Capital Investido: " + produto.getCapitalInvestido() +'\n' +
                " Taxa: " + produto.getTaxa() +'\n' +
                " Rendimento Final: " + produto.getRetorno()
        );

//        double rendimento = efetuaCalculo.getRetornoInvestimento();

//        System.out.println("Rendimento Final foi de: " + rendimento);
    }
}
