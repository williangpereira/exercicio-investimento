package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o nome do Produto: ");
        String nome = scanner.next();

        System.out.println("Digite a quantidade a quantidade de meses do investimento: ");
        int tempoInvestimento = scanner.nextInt();

        System.out.println("Digite o capital que deseja investir: ");
        double capitalInvestido = scanner.nextDouble();

        double taxa = 0.0007;

       Produto produto1 = new Produto(nome, tempoInvestimento, capitalInvestido, taxa);
       Impressora impressora = new Impressora(produto1);
    }
}
